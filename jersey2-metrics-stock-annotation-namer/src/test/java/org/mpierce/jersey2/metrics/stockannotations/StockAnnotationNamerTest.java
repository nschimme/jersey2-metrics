package org.mpierce.jersey2.metrics.stockannotations;

import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mpierce.jersey2.metrics.FixedMetricArbiter;
import org.mpierce.jersey2.metrics.MetricArbiter;
import org.mpierce.jersey2.metrics.MetricNamer;
import org.mpierce.jersey2.metrics.test.AbstractMetricsTestBase;
import org.mpierce.jersey2.metrics.test.StubNamer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class StockAnnotationNamerTest extends AbstractMetricsTestBase {
    MetricNamer metricNamer;

    @Before
    public void localSetUp() {
        metricNamer = new StockAnnotationNamer();
    }

    @Test
    public void testNoAnnotation() throws ExecutionException, InterruptedException {
        assertEquals(Response.Status.OK.getStatusCode(), req("/foo/noAnnotation").getStatusCode());
        assertNull(metricNamer.getTimerName(getEvent()));
    }

    @Test
    public void testWithAnnotation() throws ExecutionException, InterruptedException {
        assertEquals(Response.Status.OK.getStatusCode(), req("/foo/withAnnotation").getStatusCode());
        assertEquals("org.mpierce.jersey2.metrics.stockannotations.StockAnnotationNamerTest$SampleResource.withAnnotation", metricNamer.getTimerName(getEvent()));
    }

    @Override
    protected MetricArbiter getMetricArbiter() {
        // don't actually need the request to generate metrics
        return new FixedMetricArbiter(false, false);
    }

    @Override
    protected MetricNamer getMetricNamer() {
        return new StubNamer();
    }

    @Override
    protected List<Class<?>> getComponentClasses() {
        return Lists.newArrayList(SampleResource.class);
    }

    @Path("foo")
    public static class SampleResource {
        @GET
        @Path("noAnnotation")
        public String noAnnotation() {
            return "";
        }

        @GET
        @Path("withAnnotation")
        @Timed
        public String withAnnotation() {
            return "";
        }
    }
}
