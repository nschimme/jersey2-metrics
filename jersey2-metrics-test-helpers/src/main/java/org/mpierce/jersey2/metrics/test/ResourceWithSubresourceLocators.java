package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.Path;

@Path("resourceWithSubResourceLocators")
public class ResourceWithSubresourceLocators {
    @Path("srlWithSubResource")
    public ResourceWithoutPathWithSubResource getSub() {
        return new ResourceWithoutPathWithSubResource();
    }

    @Path("srlWithResource")
    public ResourceWithoutPath getRes() {
        return new ResourceWithoutPath();
    }
}
