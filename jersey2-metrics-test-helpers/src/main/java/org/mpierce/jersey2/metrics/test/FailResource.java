package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Path("fail")
public class FailResource {

    @GET
    public String get() {
        throw new RuntimeException("fail");
    }

    @GET
    @Path("WebApplicationException500")
    public String getMapped500() {
        throw new WebApplicationException(500);
    }

    @GET
    @Path("WebApplicationException400")
    public String getWebApplicationException400() {
        throw new WebApplicationException(400);
    }
    
    @GET
    @Path("Mapped400")
    public String getMapped400() {
        throw new CustomException();
    }

    public static class CustomException extends RuntimeException {
        public CustomException() {
            super("this has failed");
        }
    }

    @Provider
    public static class CustomExceptionMapper implements ExceptionMapper<CustomException> {

        public Response toResponse(CustomException ex) {
            return Response.status(400).
                    entity(ex.getMessage()).
                    type("text/plain").
                    build();
        }
    }
}
