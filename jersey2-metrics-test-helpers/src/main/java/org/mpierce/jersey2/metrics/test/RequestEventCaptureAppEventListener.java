package org.mpierce.jersey2.metrics.test;

import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;

import java.util.concurrent.BlockingQueue;

public class RequestEventCaptureAppEventListener implements ApplicationEventListener {

    private final BlockingQueue<RequestEvent> eventQueue;

    public RequestEventCaptureAppEventListener(BlockingQueue<RequestEvent> eventQueue) {
        this.eventQueue = eventQueue;
    }

    @Override
    public void onEvent(ApplicationEvent event) {
        // no op
    }

    @Override
    public RequestEventListener onRequest(RequestEvent requestEvent) {
        return new FinishedCaptureRequestEventListener(eventQueue::add);
    }
}
