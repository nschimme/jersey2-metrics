package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.GET;

public class ResourceWithoutPath {
    @GET
    public String get() {
        return "get";
    }
}
