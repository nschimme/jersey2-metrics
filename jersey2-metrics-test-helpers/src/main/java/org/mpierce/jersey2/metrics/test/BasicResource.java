package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("resource")
public class BasicResource {
    @GET
    public String get() {
        return "get";
    }
}
