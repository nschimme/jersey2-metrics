package org.mpierce.jersey2.metrics;

import org.glassfish.jersey.server.monitoring.RequestEvent;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Allows customizing which metrics get created.
 */
@ThreadSafe
public interface MetricArbiter {

    /**
     * @param event an event of {@link org.glassfish.jersey.server.monitoring.RequestEvent.Type} FINISHED
     * @return true if the request for this event should have a timer
     */
    boolean shouldHaveTimer(@Nonnull RequestEvent event);

    /**
     * @param event an event of {@link org.glassfish.jersey.server.monitoring.RequestEvent.Type} FINISHED
     * @return true if the request for this event should have a status code counter
     */
    boolean shouldHaveStatusCodeCounter(@Nonnull RequestEvent event);
}
