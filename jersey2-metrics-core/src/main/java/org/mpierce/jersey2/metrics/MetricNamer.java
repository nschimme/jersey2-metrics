package org.mpierce.jersey2.metrics;

import org.glassfish.jersey.server.monitoring.RequestEvent;

import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Allows customizing how metric names are created.
 */
@ThreadSafe
public interface MetricNamer {

    /**
     * This will only be called if {@link org.mpierce.jersey2.metrics.MetricArbiter#shouldHaveTimer(org.glassfish.jersey.server.monitoring.RequestEvent)}
     * returns true for the event.
     *
     * Because metrics are cached, you should generate consistent names (don't put a timestamp in the name or similar
     * things that will create an infinite proliferation of names).
     *
     * @param event a RequestEvent of type FINISHED
     * @return a timer name, or null if a name cannot be calculated
     */
    @Nullable
    String getTimerName(RequestEvent event);

    /**
     * This will only be called if {@link org.mpierce.jersey2.metrics.MetricArbiter#shouldHaveStatusCodeCounter(org.glassfish.jersey.server.monitoring.RequestEvent)}
     * returns true for the event.
     *
     * Because metrics are cached, you should generate consistent names (don't put a timestamp in the name or similar
     * things that will create an infinite proliferation of names).
     *
     * @param event a RequestEvent of type FINISHED
     * @return a counter name, or null if a name cannot be calculated
     */
    @Nullable
    String getStatusCodeCounterName(RequestEvent event);
}
