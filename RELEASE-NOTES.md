# 1.0.1
- Fix to avoid NPE when a resource method throws an unmapped exception. Thanks to Jaroslav Sovicka for contributing the fix!
- Update several dependencies to current versions

# 1.0.0
- Initial release