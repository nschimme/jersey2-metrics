[![Build Status](https://semaphoreapp.com/api/v1/projects/dc7649ff-7dfc-44c2-a63f-884c75057dab/317219/badge.png)](https://semaphoreapp.com/marshallpierce/jersey2-metrics)

[![IntelliJ](//www.jetbrains.com/idea/opensource/img/all/banners/idea125x37_blue.gif)](//www.jetbrains.com/idea/)

Made with an IntelliJ license for open source projects courteously provided by JetBrains.

#What is this?

It's a [Jersey 2](https://jersey.java.net/) [`ApplicationEventListener`](https://jersey.java.net/documentation/latest/monitoring_tracing.html) that captures information about your request processing in [Metrics](https://github.com/dropwizard/metrics). There's a similar [project for Jersey 1](https://github.com/palominolabs/jersey-metrics-filter) if you're still on Jersey 1.

#Why do I want it?

Suppose you have a resource like this:

```java
@Path("foo")
public class SampleResource {
    @GET
    @Path("coolData")
    public String getCoolData() {
        return calculateTheCoolestData();
    }

    ...
}
```

This library will make it so that you will automatically have the following metrics calculated for requests that invoke `SampleResource#getCoolData()` (and any other resource methods):

- A [Timer](https://dropwizard.github.io/metrics/3.1.0/manual/core/#timers) to track request handling time
- A [Counter](https://dropwizard.github.io/metrics/3.1.0/manual/core/#counters) for each status code that your resource returns (one for 200, one for 500, etc)

#How do I use it?
First, add your dependencies in `build.gradle` or your dependency mechanism of choice:

```
compile 'org.mpierce.jersey2.metrics:jersey2-metrics-core:LATEST_RELEASED_VERSION'
```

Then, when you're initializing your Jersey [ResourceConfig](https://jersey.java.net/documentation/latest/deployment.html#environmenmt.appmodel), include a `MetricsAppEventListener`:

```java
MetricsAppEventListener listener = new MetricsAppEventListener.Builder(metricRegistry).build();
myResourceConfig.register(listener);
```

This will use all default configuration; see below for the various options.

##Configuration

There are several different avenues for altering how metrics are created.

##`MetricArbiter`
A [`MetricArbiter`](https://bitbucket.org/marshallpierce/jersey2-metrics/src/master/jersey2-metrics-core/src/main/java/org/mpierce/jersey2/metrics/MetricArbiter.java?at=master) lets you control which requests have metrics calculated for them.

- [`FixedMetricArbiter`](https://bitbucket.org/marshallpierce/jersey2-metrics/src/master/jersey2-metrics-core/src/main/java/org/mpierce/jersey2/metrics/FixedMetricArbiter.java?at=master) allows the user to say "always yes" or "always no" for a given metric type. The default configuration uses this implementation in "always yes" mode.
- [`AnnotationArbiter`](https://bitbucket.org/marshallpierce/jersey2-metrics/src/master/jersey2-metrics-annotation-arbiter/src/main/java/org/mpierce/jersey2/metrics/annotation/AnnotationArbiter.java?at=master) in `jersey2-metrics-annotation-arbiter` uses the [`@ResourceMetrics` annotation](https://bitbucket.org/marshallpierce/jersey2-metrics/src/master/jersey2-metrics-annotation-arbiter/src/main/java/org/mpierce/jersey2/metrics/annotation/ResourceMetrics.java?at=master) (from this project) on resource classes or methods to control whether metrics will be created.
- [`StockAnnotationArbiter`](https://bitbucket.org/marshallpierce/jersey2-metrics/src/master/jersey2-metrics-stock-annotation-arbiter/src/main/java/org/mpierce/jersey2/metrics/stockannotations/StockAnnotationArbiter.java?at=master) in `jersey2-metrics-stock-annotation-arbiter` uses the stock `@Timed` annotation from the core Metrics project: if and only if the annotation is present, a `Timer` will be created.

Use `MetricsAppEventListener.Builder#withArbiter()` to configure which arbiter you use. If none of the above suit you, you can always write your own.

##`MetricNamer`
A [`MetricNamer`](https://bitbucket.org/marshallpierce/jersey2-metrics/src/master/jersey2-metrics-core/src/main/java/org/mpierce/jersey2/metrics/MetricNamer.java?at=master) governs how metrics are named.

- [`RequestPathMetricNamer`](https://bitbucket.org/marshallpierce/jersey2-metrics/src/master/jersey2-metrics-core/src/main/java/org/mpierce/jersey2/metrics/RequestPathMetricNamer.java?at=master) creates names based on the path of the request. The default configuration uses this implementation.
- [`StockAnnotationNamer`](https://bitbucket.org/marshallpierce/jersey2-metrics/src/master/jersey2-metrics-stock-annotation-namer/src/main/java/org/mpierce/jersey2/metrics/stockannotations/StockAnnotationNamer.java?at=master) uses the logic from [`InstrumentedResourceMethodApplicationListener`](https://github.com/dropwizard/metrics/blob/master/metrics-jersey2/src/main/java/com/codahale/metrics/jersey2/InstrumentedResourceMethodApplicationListener.java) in the core Metrics project to create names based on the method and class and `@Timed` annotation. This is equivalent to how [metrics-jersey2](https://github.com/dropwizard/metrics/tree/master/metrics-jersey2) handles `@Timed`.

Use `MetricsAppEventListener.Builder#withNamer()` to configure which namer you use. Like `MetricArbiter`s, if none of the above suit you, you can always write your own.

##Custom reservoirs
Timers have a [reservoir](https://dropwizard.github.io/metrics/3.1.0/manual/core/#uniform-reservoirs) under the hood. Different reservoirs have major effects on how your latency measurements are handled. By default, [HdrHistogramReservoir](https://bitbucket.org/marshallpierce/hdrhistogram-metrics-reservoir) is used as it's likely to be a good choice for most needs.

Use `MetricsAppEventListener.Builder#withReservoirSupplier()` to configure which reservoir implementation you use.

#Doesn't Metrics already ship something like this?
[Yes it does (`metrics-jersey2`)](https://github.com/dropwizard/metrics/tree/master/metrics-jersey2) but it's pretty limited:

- It will not generate a metric unless you annotate a method. There's no way to implement a global metric policy, so you'll need to type `@Timed` a lot.
- It doesn't support global naming policies, You have to manually set the `name` of each annotation if you don't like what it makes for you out of the box.
- It doesn't support custom reservoirs. You almost certainly want to use a non-default reservoir like [this one](https://bitbucket.org/marshallpierce/hdrhistogram-metrics-reservoir).

By using `StockAnnotationNamer` and `StockAnnotationArbiter` you are effectively recreating `metrics-jersey2` for timers. `metrics-jersey` also supports `@Metered` and `@ExceptionMetered`, which this library does not. If this is an important feature for you, [file an issue](https://bitbucket.org/marshallpierce/jersey2-metrics/issues?status=new&status=open)!
