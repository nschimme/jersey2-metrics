package org.mpierce.jersey2.metrics.benchmark;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("multiResponseCode")
public final class MultiResponseCodeResource {

    /**
     * Don't care if this has mad crazy data races. I just want it to be cheap and to change every now and then.
     */
    volatile int nextCode = 200;

    @GET
    public Response get() {
        int code = nextCode++;

        if (code > 205) {
            nextCode = 200;
            code = 200;
        }

        return Response.status(code).entity("foo").build();
    }
}
